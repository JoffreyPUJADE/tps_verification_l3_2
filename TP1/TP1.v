(* TD/TP N°1 *)

(* Exercice N°1 *)

(* Q1 *)

Parameters A B : Prop.
Lemma Q1_1 : A -> B -> A.
Proof.
intro.
intro.
assumption.
Qed.

(* Q2 *)

Parameters C : Prop.
Lemma Q2_1 : (A -> B -> C) -> (A -> B) -> A -> C.
Proof.
intro.
intro.
intro.
apply H.
apply H1.
apply H0.
assumption.
Qed.

(* Q3 *)

Lemma Q3_1 : A /\ B -> B.
Proof.
intro.
elim H.
intro.
intro.
assumption.
Qed.

(* Exercice N°2 *)

(* Q1 *)

Parameter E : Set.
Parameters P Q : E -> Prop.
Lemma Q1_2 : forall x : E, P(x) -> exists y : E, P(y) \/ Q(y).
Proof.
intro.
intro.
exists x.
left.
assumption.
Qed.

(* Q2 *)

Lemma Q2_2 : (exists x : E, P(x) \/ Q(x)) -> (exists x : E, P(x)) \/ (exists x : E, Q(x)).
Proof.
intro.
elim H.
intro.
intro.
elim H0.
intro.
left.
exists x.
assumption.
intro.
right.
exists x.
assumption.
Qed.

(* Q3 *)

Lemma Q3_2 : (forall x : E, P(x)) /\ (forall x : E, Q(x)) -> forall x : E, P(x) /\ Q(x).
Proof.
intro.
elim H.
intro.
intro.
intro.
split.
apply H0.
apply H1.
Qed.